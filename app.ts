

import { bootstrap } from "@angular/platform-browser-dynamic";
import { Component } from "@angular/core";

@Component({
  selector: 'calc-main',
  templateUrl: 'app/calc-main.component.html'
})
export class CalcMain {
	lum_options: number[] = [375, 600, 900, 1125, 1600];
	lum: number;
	cents: number;
	hours: number;
	total_days: number = 365;
	total_hours: number;
	cost: number;

	inc_conversion: number = .0625;
	hal_conversion: number = .0450;
	cfl_conversion: number = .0146;
	led_conversion: number = .0125;

	inc_wattage: string;
	hal_wattage: string;
	cfl_wattage: string;
	led_wattage: string;

	inc_cost: string;
	hal_cost: string;
	cfl_cost: string;
	led_cost: string;

	constructor() {
		this.lum = 375;
		this.cents = 12;
		this.hours = 3;
		this.calculate(this.lum, this.cents, this.hours);
	}

	calculate(lum:number, cents:number, hours:number): void {
		this.inc_wattage = (lum * this.inc_conversion).toFixed(1);
		this.hal_wattage = (lum * this.hal_conversion).toFixed(1);
		this.cfl_wattage = (lum * this.cfl_conversion).toFixed(1);
		this.led_wattage = (lum * this.led_conversion).toFixed(1);

		if (hours > 24) { hours = 24; }

		this.total_hours = this.total_days * hours;
		this.cost = cents / 100;

		this.inc_cost = (((Number(this.inc_wattage) * this.total_hours) / 1000) * this.cost).toFixed(2);
		this.hal_cost = (((Number(this.hal_wattage) * this.total_hours) / 1000) * this.cost).toFixed(2);
		this.cfl_cost = (((Number(this.cfl_wattage) * this.total_hours) / 1000) * this.cost).toFixed(2);
		this.led_cost = (((Number(this.led_wattage) * this.total_hours) / 1000) * this.cost).toFixed(2);
		
	}
	
}

bootstrap(CalcMain);
